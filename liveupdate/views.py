from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers

from django.views.generic.list import ListView

from liveupdate.models import *

# Create your views here.

class UpdateListView(ListView):
    model = Update

def updates_after(request, id):
    response = HttpResponse()
    response['Content-Type'] = 'text/javascript'
    response.write(serializers.serialize('json',
        Update.objects.filter(pk__gt=id)))
    return response
