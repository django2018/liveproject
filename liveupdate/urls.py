from django.conf.urls import url
from liveproject.liveupdate import views

urlpatterns = [
    url(r'^$', views.UpdateListView.as_view()),
    url(r'^updates-after/(?P<id>\d+)/$', views.updates_after),
]